import React from 'react';

const OrderListItemLoading = () => {
    return (
        <div className="item">
            <div className="ui placeholder">
                <div className="image header">
                    <div className="line"></div>
                    <div className="line"></div>
                </div>
            </div>
        </div>
    );
} 

export default OrderListItemLoading;