import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import ordersApi from '../../apis/ordersApi';
import history from '../../history';

const OrderEdit = props => {

    const [order, setOrder] = useState();
    const [loading, setLoading] = useState(true);
    const [newAddress, setNewAddress] = useState('');

    useEffect(() => {
        const fetchOrder = async () => {
            setLoading(true);
            const res = await ordersApi.get(`orders?id=${props.match.params.id}`);
            setOrder(res.data[0]);
            setNewAddress(res.data[0].address);
            setLoading(false);
        }
        fetchOrder();
    }, [props.match.params.id]);

    const updateAddress = async () => {
        await ordersApi.patch(`/orders/${order.id}`, { 'address': newAddress }, {
            headers: {
                'content-type': 'application/json'
            }
        });
        history.push(`/orders/${order.id}`);
    }

    if (loading) {
        return <div>Loading...</div>;
    }
    return (
        <div>
            <h3>What is the new address for Order #{order.id}?</h3>
            <div className="ui input" style={{width: '100%'}}>
                <input onChange={e => setNewAddress(e.target.value)} value={newAddress} type="text" />
            </div>
            <div style={{marginTop: '16px'}}>
                <button className="ui button black" onClick={() => updateAddress()}>
                    Update
                </button>
                <Link to={`/orders/${order.id}`} className="ui button">
                    Cancel
                </Link>
            </div>
         </div>
    );
}

export default OrderEdit;