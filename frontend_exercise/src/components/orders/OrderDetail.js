import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'; 

import ordersApi from '../../apis/ordersApi';
import OrderItemList from '../orderItems/OrderItemList';

const OrderDetail = props => {

    const [order, setOrder] = useState();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchOrder = async () => {
            setLoading(true);
            const res = await ordersApi.get(`orders?id=${props.match.params.id}`);
            setOrder(res.data[0]);
            setLoading(false);
        }
        fetchOrder();
    }, [props.match.params.id]);

    const renderEdit = () => {
        if (order.status !== 'shipped') {
            return (
                <Link style={{marginTop: '8px'}} to={`/orders/edit/${order.id}`} className="ui button black">
                    Edit Address
                </Link>   
            );
        }
    }

    if (loading) {
        return <div>Loading...</div>;
    }
    return (
        <div>
            <h2 className="ui center aligned icon header">
                <i className="shopping bag circular icon"></i>
                <div className="content">
                    Order #{order.id}
                    <div className="sub header" style={{marginBottom: '10'}}>{order.checkoutAt}</div>
                    <div className="sub header">{order.address}</div>
                    {renderEdit()}
                </div>
            </h2>
            <div className="center aligned" style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <div className="large ui label">
                    <i className="shipping fast icon"></i>
                    {order.status}
                </div>
                <div className="large ui label">
                    <i className="mail icon"></i>
                    {order.email}
                </div>
                <div className="large ui label">
                    <i className="pound sign icon"></i>
                    {order.total}
                </div>
            </div>
            <OrderItemList orderId={order.id} />
        </div>
    )
}

export default OrderDetail;