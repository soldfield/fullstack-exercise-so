import React from 'react';

const OrderListLoading = () => {

    const renderList = () => {
        const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        return array.map(item => {
            return (
                <div className="item" key={item}>
                    <div className="ui placeholder">
                        <div className="image header">
                            <div className="line"></div>
                            <div className="line"></div>
                        </div>
                    </div>
                </div>
            );
        });
    }
    return (
        <div className="ui relaxed middle aligned divided selection list">
            {renderList()}
        </div>
    );
}

export default OrderListLoading;