import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import ordersApi from '../../apis/ordersApi';
import OrderListItemLoading from './OrderListItemLoading';

const OrderListItem = ({ order }) => {

    const [orderItems, setOrderItems] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchOrderItems = async () => {
            setLoading(true);
            const res = await ordersApi.get(`orderitems?orderId=${order.id}`);
            setOrderItems(res.data);
            setLoading(false);
        }
        fetchOrderItems();
    }, [order]);

    const calculateQuantity = () => {
        const totalQuantity = orderItems.reduce((prev, cur) => {
            return prev + cur.quantity;
          }, 0);
        return totalQuantity;
    }

    if (loading) {
        return <OrderListItemLoading />
    }
    return (
        <Link to={`/orders/${order.id}`} className="item" key={order.id}>
            <div className="right floated content" style={{textAlign: 'right'}}>
                <div className="header">£{order.total}</div>
                <div className="description">{order.status}</div>
            </div>
            <i className="large middle aligned shopping bag icon"></i>
            <div className="content">
                <div className="header">Order #{order.id} - {order.email}</div>
                <div className="desciption">Item Quantity: {calculateQuantity()}</div>
            </div>
        </Link>
    );
}

export default OrderListItem;