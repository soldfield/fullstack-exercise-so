import React, { useEffect, useState } from 'react';

import ordersApi from '../../apis/ordersApi';
import OrderListItem from './OrderListItem';
import OrderListLoading from './OrderListLoading';

const OrderList = () => {

    const [orders, setOrders] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchOrders = async () => {
            setLoading(true);
            const res = await ordersApi.get('orders');
            setOrders(res.data);
            setLoading(false);
        }
        fetchOrders();
    }, []);

    const renderList = () => {
        return orders.map(order => {
            return (
                <OrderListItem order={order} key={order.id} />
            );
        });
    }

    if (loading) {
        return <OrderListLoading />;
    }
    return (
        <div className="ui relaxed middle aligned divided selection list">
            {renderList()}
        </div>
    );
}

export default OrderList;