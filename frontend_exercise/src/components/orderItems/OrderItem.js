import React, { useEffect, useState } from 'react';

import ordersApi from '../../apis/ordersApi';

const OrderItem = ({ orderItem }) => {

    const [product, setProduct] = useState();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchProduct = async () => {
            setLoading(true);
            const res = await ordersApi.get(`products/${orderItem.productId}`);
            setProduct(res.data);
            setLoading(false);
        }
        fetchProduct();
    }, [orderItem]);

    if (loading) {
        return <div>Loading...</div>;
    }
    return (
        <div className="item" key={orderItem.id}>
            <div className="right floated content" style={{textAlign: 'right'}}>
                <div className="header">Price: £{orderItem.itemPrice}</div>
            </div>
            <i className="large middle aligned tag icon"></i>
            <div className="content">
                <div className="header">{orderItem.quantity}x {product.title}</div>
            </div>
        </div>
    );
}

export default OrderItem;