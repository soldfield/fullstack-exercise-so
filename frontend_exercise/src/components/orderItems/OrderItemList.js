import React, { useState, useEffect } from 'react';

import ordersApi from '../../apis/ordersApi';
import OrderItem from './OrderItem';

const OrderListItem = ({ orderId }) => {

    const [orderItems, setOrderItems] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchOrderItems = async () => {
            setLoading(true);
            const res = await ordersApi.get(`orderitems?orderId=${orderId}`);
            setOrderItems(res.data);
            setLoading(false);
        }
        fetchOrderItems();
    }, [orderId]);

    const renderList = () => {
        return orderItems.map(orderItem => {
            return <OrderItem orderItem={orderItem} key={orderItem.id} />
        });
    }

    if (loading) {
        return <div>Loading...</div>;
    }
    return (
        <div>
            <h2 className="ui header" style={{marginTop: '18px'}}>Order Items</h2>
            <div className="ui relaxed middle aligned divided selection list">
                {renderList()}
            </div>
        </div>
    );
}

export default OrderListItem;