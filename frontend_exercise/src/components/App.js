import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from '../history';
import Header from './Header';
import NotFound from './NotFound';
import OrderList from './orders/OrderList';
import OrderDetail from './orders/OrderDetail';
import OrderEdit from './orders/OrderEdit';

const App = () => {
	return (
		<div className="ui container">
			<Router history={history}>
				<Header />
				<Switch>
					<Route path="/" exact component={OrderList} /> 
					<Route path="/orders/:id" exact component={OrderDetail} /> 
					<Route path="/orders/edit/:id" exact component={OrderEdit} /> 
					<Route component={NotFound} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
