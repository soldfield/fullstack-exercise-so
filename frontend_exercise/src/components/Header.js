import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="ui secondary pointing menu" style={{marginBottom: '30px'}}>
            <Link to="/" className="active item">
                Orders
            </Link>
            <div className="right menu">
                <div className="ui item">
                    <img alt="everpress logo" src="/everpress-logo.png" />
                </div>
            </div>
        </div>
    );
}

export default Header;