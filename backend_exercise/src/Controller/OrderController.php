<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Faker;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;

class OrderController extends AbstractController
{
    /**
     * @Route("/orders", name="get_all_orders", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $orders = $this->getDoctrine()
            ->getRepository(Order::class)
            ->findAll();

        $data = [];

        foreach ($orders as $order) {
            $data[] = $this->generateOrderData($order);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/orders/{id}", name="get_one_order", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $order = $this->getDoctrine()
            ->getRepository(Order::class)
            ->findOneBy(['id' => $id]);

        if (!$order) {
            throw new NotFoundHttpException('Order requested does not exist.');
        }

        $data = $this->generateOrderData($order);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/orders", name="add_order", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $email = $request->request->get('email');

        if (empty($email)) {
            throw new NotFoundHttpException('Request missing mandatory email parameter.');
        }

        $emailConstraint = new Assert\Email();
        $emailConstraint->message = 'Invalid email address.';

        $errors = $validator->validate(
            $email,
            $emailConstraint
        );

        if (count($errors) > 0) {
            $errorMessage = $errors[0]->getMessage();
            throw new BadRequestHttpException($errorMessage);
        }

        $checkout = new \DateTime();

        $order = new Order();
        $order->setEmail($email);
        $order->setCheckout($checkout);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($order);
        $entityManager->flush();

        return new JsonResponse(['status' => 'New order created with id of ' . $order->getId()], Response::HTTP_CREATED);
    }

    /**
     * @Route("/orders/{id}", name="delete_order", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $order = $this->getDoctrine()
            ->getRepository(Order::class)
            ->findOneBy(['id' => $id]);

        if (!$order) {
            throw new NotFoundHttpException('Order cannot be deleted as it does not exist.');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $orderItems = $order->getOrderItems();
        foreach ($orderItems as $orderItem) {
            $entityManager->remove($orderItem);
        }

        $entityManager->remove($order);
        $entityManager->flush();

        return new JsonResponse(['status' => 'Order deleted successfully'], Response::HTTP_NO_CONTENT);
    }

    public function generateOrderData($order)
    {
        $orderItems = $order->getOrderItems();
        $totalPrice = 0;
        $totalQuantity = 0;
        $items = [];

        foreach ($orderItems as $orderItem) {
            $orderItemPrice = $orderItem->getQuantity() * $orderItem->getPrice();
            $totalPrice += $orderItemPrice;
            $totalQuantity += $orderItem->getQuantity();
            $items[] = [
                'product' => $orderItem->getProduct()->getName(),
                'quantity' => $orderItem->getQuantity(),
                'price' => $orderItem->getPrice()
            ];
        }

        $data = [
            'id' => $order->getId(),
            'email' => $order->getEmail(),
            'checkout' => $order->getCheckout(),
            'totalPrice' => $totalPrice,
            'totalQuantity' => $totalQuantity,
            'orderItems' => $items
        ];

        return $data;
    }

    /**
     * @Route("/test-orders", name="create_test_orders")
     */
    public function createTestOrders(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

//        Fetches all products
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        $faker = Faker\Factory::create();

//        Creates 20 random orders
        for ($x = 0; $x < 20; $x++) {
            $order = new Order();
            $order->setEmail($faker->email);
            $order->setCheckout($faker->dateTime);

            $orderItem = new OrderItem();
            $orderItem->setOrderEntity($order);
            $orderItem->setProduct($products[rand(0, 2)]);
            $orderItem->setQuantity(rand(1, 2));
            $orderItem->setPrice(rand(10, 20));

            $entityManager->persist($order);
            $entityManager->persist($orderItem);
        }

//        Saves the orders and order items to the DB
        $entityManager->flush();

        return new Response('Saved 20 new orders to the database');
    }
}
