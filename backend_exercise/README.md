## Everpress Backend Exercise - PHP

## Getting Started

We are assuming you already have PHP 7 installed locally.

1. Install [Symfony command line](https://symfony.com/download).

2. Log in to your Bitbucket account and set up an SSH key following the instructions [here](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/).

3. Clone the repository

```bash
git clone git@bitbucket.org:project-development/backend-exercise-CANDIDATES_INITIALS.git
```

4. Install third party packages

```bash
composer install
```

5. Start the webserver using

```bash
symfony server:start
```

6. Run database migrations while the server is running to create and populate the Product entity.

```bash
bin/console doctrine:migrations:migrate
```

Notes to remember:

- It's ok if you don't complete all the tasks
- You will have an opportunity to tell us what you would have done if given more time
- You're free to install any packages you need for the task
- You're free to add features/improvements not listed below
- Try to have at least one commit per task so that we can see the work progress
- The API should be designed so that it could be used by someone that doesn't know the database structure
- Everpress is growing quickly and we've passed 1,000,000 orders this year
- No authentication required for this API, but think about how would you add it in the future

## Tasks

### 1. Ability to store Orders with Order Items

A [Product](src/Entity/Product) entity has already been created.

- Add entities to store Orders with many Order Items of Products
- Orders entity must have fields for `email` and `checkout` datetime
- Order Item entity must have fields for `quantity` and `price` (price should be per item) and relation for which product
  they ordered

### 2. API to list all Orders

- Using your newly created entities create GET API for returning all orders in JSON
- This should include the _total_ price and quantity for each order
- The total price for an order item should be `price * quantity`

### 3. API to create Order

- Create POST API for adding a new Order and populating the `email` field