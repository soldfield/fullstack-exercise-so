<?php

namespace App\Tests\Controllers;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class OrderControllerTest extends TestCase
{
    private $http;

    public function setUp(): void
    {
        $this->http = new Client(['base_uri' => 'localhost:8000']);
    }

    public function tearDown(): void
    {
        $this->http = null;
    }

    public function testGetAllEndpointReturnsSuccessfulResponse() {
        $response = $this->http->request('GET', '/orders');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetEndpointIsSuccessfulIfOrderExists() {
        $response = $this->http->request('GET', '/orders/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetEndpointReturns404IfOrderDoesNotExist() {

        $response = $this->http->request('GET', '/orders/99999', [
            'http_errors' => false
        ]);
        $this->assertEquals(404, $response->getStatusCode());

    }

    public function testAddEndpointWorksWithValidEmail() {
        $response = $this->http->request('POST', '/orders', [
            'form_params' => [
                'email' => 'realemail@email.com'
            ]
        ]);
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testAddEndpointFailsWithInvalidEmail() {
        $response = $this->http->request('POST', '/orders', [
            'form_params' => [
                'email' => 'notarealemail'
            ],
            'http_errors' => false
        ]);
        $this->assertEquals(400, $response->getStatusCode());
    }

}